//
//  Album.swift
//  TableViewTutorial
//
//  Created by Brian Lane on 11/10/19.
//  Copyright © 2019 Brian Lane. All rights reserved.
//

import Foundation

struct Album {
    let name: String
    let artist: String
    let genre: String
}

extension Album {
    static func generateTestAlbums() -> [Album]{
        let artists = ["Eleanor Jones", "Mary Coltrane", "Elizabeth Davis", "Junie B. Bowie", "Penelope Lennon", "Frances McCartney", "Dianne Gruberberg", "Hailey Donovan", "Ellen John"]
        let albumTitles = ["Colonel Salt's Missed Connections Combo", "Let it Stay", "Kind of Pale", "A Love Okay", "Witches Woo", "Out to Brunch!", "Mona-in'", "Live at a Cool Place '59", "Live at a Really Cool Place '60", "Um Ah Yeah Um Right", "Timpani Colossus", "Sketches of Plane", "Cool Jazz", "Hip Jazz", "Bad Jazz", "On the Corner (Again)"]
        let genre = "jazz"
        
        var albums = [Album]()
        for title in albumTitles {
            let artistName = artists.randomElement()!
            let album = Album(name: title, artist: artistName, genre: genre)
            albums.append(album)
        }
        return albums
    }
}
